# Алгоритм работы с проектом в GIT и GitLab 

- [Начало работы с проектом](#start)
- [Создание новой ветки для работы над задачей](#creating-branch)
- [Завершение работы над задачей, подготовка ветки к merge request](#preparing-merge)
- [Запрос на слияние(merge request) рабочей ветки с веткой `develop`](#merge-request)


## Начало работы с проектом <a name = "start"></a>

- Клонируем репозиторий

```
git clone ссылка на репозиторий (например, git clone https://gitlab.com/god15/projects/name-project/project.git)
```

## Создание новой ветки для работы над задачей <a name = "creating-branch"></a>

- Основная рабочая ветвь `develop`. Обновляем локальную ветку `develop`

```
git pull origin develop
```

 - Все новые ветки создаем из `develop`. Нейминг веток должен быть в соответствии с [Git Flow](https://tropical-railway-c8b.notion.site/GIT-FLOW-8f6aa0f2e10c43cab89ccba9311dd713#312a35fa244946afa1fac0005ea833b3). Создаем новую рабочую ветку и переключаемся на неё

 ```
git checkout -b 'название рабочей ветки'
 ```
 Коммиты делаем через текстовый редактор и называем в соответствии с [Git Flow](https://tropical-railway-c8b.notion.site/GIT-FLOW-8f6aa0f2e10c43cab89ccba9311dd713#312a35fa244946afa1fac0005ea833b3)

## Завершение работы над задачей, подготовка ветки к merge request <a name = "preparing-merge"></a>

- Отправляем изменения с нашей рабочей локальной ветки в удаленную(НЕ develop)

 ```
git push origin 'название рабочей ветки'
 ```

Пока ведется работа над задачей коллеги вносят изменения в удаленную ветку `develop`, их нужно учесть перед merge request

- Подтягиваем изменения из удаленной ветки `develop` в нашу локальную рабочую(НЕ develop) ветку

```
git pull origin develop
 ```

- Решаем конфликты локально в редакторе, после чего создаем коммит и еще раз отправляем изменения с нашей рабочей локальной ветки в удаленную(НЕ develop). Только после этого можно делать merge request

```
git push origin 'название рабочей ветки'
 ```

## Запрос на слияние(merge request) рабочей ветки с веткой `develop` <a name = "merge-request"></a>

- Запрос на слияние создается в удаленном репозитории проекта на GitLab. После отправки изменений в удаленную рабочую ветку GitLab сам предложит создать merge request

![alt text](img/image10.png)


- Либо для самостотельного создания merge request в левом боковом меню выбираем `Merge request`, затем в правом верхнем углу `New merge request`

![alt text](img/image1.png)



### ВАЖНО  создать `merge request` именно в ветку `develop`



![alt text](img/image13.png)

- Название и описание merge request нужно заполнить в соответствии с [Git Flow](https://tropical-railway-c8b.notion.site/GIT-FLOW-8f6aa0f2e10c43cab89ccba9311dd713#312a35fa244946afa1fac0005ea833b3)

![alt text](img/image14.png)